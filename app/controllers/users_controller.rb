require 'open-uri'
require 'nokogiri'
require 'uri'
require 'iconv'
require 'cgi'

class UsersController < ApplicationController

  def show
    @user = User.includes(:vote_options).find_by_id(params[:id])
  end

  def new
  end

  def change
    user = User.where(name: user_params[:name]).take
  end

  def create
    user = User.new(user_params)
    name = user_params[:name]

    conv = Iconv.new('EUC-KR','UTF-8')
    query = conv.iconv(name)
    de_name = CGI::escape(query)

    #이름을 주소에 적용해 웃대에 답글 검색을 한다.
    one = Nokogiri::HTML(open("http://web.humoruniv.com/board/humor/comment_search.html?sort=mtime&searchday=all&sk=#{de_name}"))
    #아이콘을 찾아 그 안의 아이콘 위치 주소를 얻는다.
    #hu_icon = one.xpath("//img[@class='hu_icon']")[0]["src"]
    hu_icon = one.xpath("//img[@class='hu_icon']")[0]



    if !hu_icon.nil? && User.where(name: name).take.nil?
      if user.save
        session[:user_id] = user.id

        setting = UserSetting.new
        setting.user = user
        setting.ip = request.remote_ip
        setting.is_admin = false
        if (UserSetting.where(ip: request.remote_ip).take.nil?)
          setting.is_ban = false
        else
          setting.is_ban = true
        end

        setting.save

        redirect_to '/'
      else
        redirect_to '/signup'
      end
    else
      redirect_to '/signup'
    end

  end

  private

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation)
  end

end