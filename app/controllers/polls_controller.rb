class PollsController < ApplicationController
  before_action :authorize
  def index
    @polls = Poll.order('created_at DESC')
    @user_setting = UserSetting.find_by_user_id(current_user)
  end

  def info
    @user = User.order('created_at DESC')
    @remote_ip = request.remote_ip.split('.')[0]
    @idols = IdolList.order('created_at DESC')
  end

  def show
    @poll = Poll.includes(:vote_options).find_by_id(params[:id])
    @vote = Vote.new
    @user_setting = UserSetting.find_by_user_id(current_user)
    #@idol = IdolList.where(name: option.title).take.image
  end

  def new
    @poll = Poll.new
  end

  def edit
    @poll = Poll.find_by_id(params[:id])
  end

  def update
    @poll = Poll.find_by_id(params[:id])
    if @poll.update_attributes(poll_params)
      flash[:success] = '투표가 업데이트되었습니다!'
      redirect_to polls_path
    else
      render 'edit'
    end
  end

  def create
    @poll = Poll.new(poll_params)
    if @poll.save
      flash[:success] = '투표가 새로 만들어졌습니다!'
      redirect_to polls_path
    else
      render 'new'
    end
  end

  def destroy
    @poll = Poll.find_by_id(params[:id])
    if @poll.destroy
      flash[:success] = '투표가 삭제되었습니다!'
    else
      flash[:warning] = '투표를 삭제하는 과정에서 오류가 생겼습니다....'
    end
    redirect_to polls_path
  end

  private

  def poll_params
    params.require(:poll).permit(:topic, :close_time, vote_options_attributes: [:id, :title, :image, :_destroy])
  end
end