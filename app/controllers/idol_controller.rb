class IdolController < ApplicationController
  def create
    if current_user && params[:poll] && params[:poll][:id] && params[:vote_option] && params[:vote_option][:id]
      @poll = Poll.find_by_id(params[:poll][:id])
      @option = @poll.vote_options.find_by_id(params[:vote_option][:id])
      if @option && @poll && !current_user.voted_for?(@poll)
        current_user.votes.create({vote_option_id: @option.id})
      else
        render js: 'alert(\'이미 투표가 되어있어 저장이 되지 않습니다.\');'
      end
    else
      render js: 'alert(\'투표가 저장되지 않았습니다.\');'
    end
  end

  def info
  end
end
