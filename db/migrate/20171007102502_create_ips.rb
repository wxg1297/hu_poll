class CreateIps < ActiveRecord::Migration
  def change
    create_table :ips do |t|
      t.references :user, index: true
      t.integer :ip, default: 0

      t.timestamps
    end
  end
end
