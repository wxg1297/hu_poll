# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
File.open("db/input.txt", "r") do |infile|
  while (line = infile.gets)
    img = line.split(",")[0].strip
    n = line.split(",")[1].strip
    IdolList.create(name: n, image: img)
  end
end


